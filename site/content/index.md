---
title: TCE-AM/DINFRA
author: Diego F. Nascimento
---
# Sobre

A infraestrutura de TI consiste em todos os elementos que suportam o gerenciamento e a usabilidade de dados e informações. Estes incluem o hardware físico e instalações, como data centers, armazenamento e recuperação de dados, sistemas de rede, interfaces legadas e software para suportar os objetivos de negócio de uma empresa. A estrutura também inclui contratação, treinamento, políticas, testes, processos, atualizações e reparos.

A Divisão de Infraestrutura de Tecnologia da Informação do TCE/AM - DITEC - tem como objetivo garantir o bom funcionamento de todos estes componentes, atuando de forma direta ou indireta, através da elaboração e implantação de projetos técnicos, planejamento e execução de manutenções preventivas e corretivas e monitoramento dos sistemas de hardware e software.


### Infraestrutura de TI no TCE/AM

A infraestrutura de T.I. do Tribunal de Contas do Estado do Amazonas está organizada da seguinte forma:


### Componentes de Datacenter

   * UPS (Nobreaks) – do inglês, Uninterrupted Power System, este componente garante a alimentação do datacenter em caso de falta de energia e falha no Gerador externo.

   * Servidores Físicos – São computadores com grande capacidade de processamento e memória e são utilizados para execução de sistemas em geral. Dentre eles, alguns são servidores de rack e alguns fazem parte do sistema de Blades da 	IBM. Sendo estes, utilizados exclusivamente para execução do sistema de virtualização (VMWare).

   * Virtualizador (VMWare) – O virtualizador é um software capaz de emular o hardware necessário para execução de sistemas operacionais utilizando uma quantidade bem definida de memória e processamento. Ou seja, o hardware sobre o qual está sendo executado é compartilhado por vários servidores virtuais menores. (Obs.: este sistema é essencial para se obter um bom aproveitamento do hardware disponível)

   * Servidores Virtuais – Estes, conhecidos como Máquinas Virtuais, são utilizados para execução de aplicações em geral desta corte de contas. Dentre elas, também é executado um cluster de “containers linux” Kubernetes.

   * Cluster de Containers (Kubernetes/Docker) – O 	Kubernetes (K8S), é um sistema capaz de executar aplicações em ambientes isolados (containers), garantindo maior flexibilidade e segurança na execução de sistemas em rede. O Cluster é composto por várias máquinas físicas e/ou virtuais (nós) e sua capacidade de processamento e memória são proporcionais ao número de nós disponíveis. Ou seja, para se ampliar a capacidade operacional, basta adicionar nós ao cluster.

   * Tape Library (IBM) – A Tape Library é utilizada para armazenamento de Backups em Fitas especialmente projetadas para tal tarefa.

   * Storage (IBM) – Este basicamente é um sistema integrado de armazenamento. É composto	por uma controladora e vários discos. Todos os dados do TCE/AM são armazenados nesses dispositivos.

   * Sistema de Backup (ITSM – Tivoli Storage Manager) – Este é um sitema de software responsável por efetuar o Backup dos dados e armazená-los na Tape Library.

   * Sistemas de Monitoramento (Strucxure) – Componente de software integrado à vários sensores dentro do datacenter que permite o monitoramento de temperatura, consumo de energia, humidade, dentre outros dados.

### Componentes de Rede

   * Switchs (Dell) – São equipamentos 	(hardware) utilizados para interligação e extensão dos pontos de rede. Atualmente o tribunal possui 60 switchs modelo 2048P distribuídos em 10 salas de telecom (TR) e 15 Racks de Parede.

   * Access Points – Equipamentos de rede sem fio (Wi-Fi).

   * Roteadores – Estes são utilizados para interligação de redes distintas. No TCE, são utilizados 2 roteadores, ambos para interligação da rede do TCE com a Internet.

   * Links de Internet (Embratel e Metromao) – O TCE possui 2 links de internet, sendo um com capacidade de 300Mbps, provido pela Embratel, e outro com capacidade de N Mbps, provido pela PRODAM.

   * Cabeamento Estruturado – Cabeamento físico que interligam todos os equipamentos de informática do TCE/AM.

   * Firewall – Equipamento de segurança de rede responsável por monitorar e filtar pacotes de rede. Atualmente o TCE possui dois firewalls Palo Alto de última geração.

### Componentes de Usuário

   * Impressoras – Todas as impressoras do Tribunal são mantidas pela empresa terceira Fullcopy.

   * Estações de Trabalho (Pcs) – Computadores utilizados pelos usuários.



