---
title: Sistemas
author: Diego F. Nascimento
---
# Sistemas

A **SETIN** disponibiliza aplicações e serviços internos e externos. Os serviços internos são aqueles utilizados apenas por servidores desta Corte de Contas, enquanto que os externos são desenvolvidos para atender os jurisdicionados e/ou o público em geral. Abaixo seguem os sistemas disponibilizados em nossa infraestrutura.


   * **SPEDE** - Sistema de Processo Finalístico (interno) [http://spede.tce.am.gov.br]
      * Componentes: Servidor Web (10.46.30.1), Banco de Dados (spededb.tce.am.gov.br)
   * **Julgamento** – Sistema de Julgamento integrado ao SPEDE (interno) 	[https://julgamento.tce.am.gov.br]
   * **Sistema de Processo** – Administrativo – SEI (interno) [https://sei.tce.am.gov.br]
   * **e-Contas** – Prestação de Contas (externo) 	[https://econtas.tce.am.gov.br]
   * **Portal RH** (interno) [https://portalrh.tce.am.gov.br]
   * **SAP** (descontinuado) 	
   * **Sistema de Ouvidoria** (externo) [https://ouvidoria.tce.am.gov.br]
   * **Portais** (externos)
      * Site do TCE - https://www.tce.am.gov.br
      * Portal da Intranet - https://intranet.tce.am.gov.br
      * Portal do Diário Oficial - https://doe.tce.am.gov.br
      * Portal da Escola de Contas - https://ecp.tce.am.gov.br
      * Portal de Secretaria de Controle Externo - https://secex.tce.am.gov.br
      * Portal da Ouvidoria - https://ouvidoria.tce.am.gov.br
      * Portal do SEI (Sistema de Processos Administrativos) - https://portalsei.tce.am.gov.br
      * Portal Simpósio Internacional de Gestão Ambiental - https://sigam.tce.am.gov.br
      * Portal de Suporte da SETIN - https://suporte.tce.am.gov.br
      * Portal da Transparência - https://transparencia.tce.am.gov.br	
   * SSE – Sistema de Seleção de Estagiários (inativo)
   * Serviços – Acesso a informações sobre processos (externo) 	[https://servicos.tce.am.gov.br]
   * API TCE – Serviço que disponibiliza os dados necessários para o Aplicativo do TCE/AM. para dispositivos móveis; (externo) [https://api.tce.am.gov.br]
   * Repositório de Código Fonte – (interno) [https://gitlab.tce.am.gov.br]

