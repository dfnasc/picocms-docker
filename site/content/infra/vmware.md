# VMWare


## Instalação do vSphere Replication Appliance

Para cada site, deve-se executar o seguintes passos:

1. Download da imagem ISO do Appliance no site da vmware ([My VMWare](https://my.vmware.com/)
2. Remover usuário do vSphere Replicatioan, caso exista. VCenter > Administration > Users and Groups > Groups > Solution Users
3. Desregistrar a extensão do vSphere Replication, caso exista. 
   - Acesse o endereço https://<vcenter-address>/mob/?moid=ExtensionManager
   - clique no link "UnregisterExtension"
   - Adicione "com.vmware.vcHms" no valor da chave e clique em "Invoke Method"
   - O resultado deve exibir "void"
4. Faça o deploy do vSphere Replication:
   - monte a ISO (ex: mount <arquivo>.iso /mnt -o loop)
   - selecione os arquivos:
      * vSphere_Replication_OVF10.cert
      * vSphere_Replication_OVF10.mf
      * vSphere_Replication_OVF10.ovf
      * vSphere_Replication-support.vmdk
      * vSphere_Replication-system.vmdk
5. Ligue a máquina virtual gerada e acesse o endereço do vSphere Replication: (Ex: https://<vsphere-replication-address>:5480)
6. Selecione a aba "Configuration" e preencha o formulário da seguinte forma:
   * **Configuration Mode:**: selecione a opção "Configure using the embedded database"
   * **LookupServiceAddress:** informe o endereço do vcenter no qual o VR atuará
   * **SSO Administrator:** informe o nome do usuário administrador do vcenter (Ex.: Administrator@vsphere.local)
   * **Password:** informe a senha do usuário administrador do vcenter;
   * **VRM Host:** informe o endereço do vSphere Replication;
   * **VRM Site Name:** informe um nome para site onde o vSphere Replication atuará;
   * **vCenter Server Address:** informe o endereço do vcenter;
   * **vCenter Server Port:** 80;
   * **vCenter Server Admin Mail:** informe o e-mail do administrador do vcenter. (qualquer e-mail)
7. Clique no botão "Save and Restart Service"
