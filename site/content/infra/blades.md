# IBM Blade System

Atualmente nosso sistema de blades da IBM está dividido em 2 racks integrados, porém gerenciados separadamente.

Cada rack possui um conjunto de lâminas de processamento e um [Storage V7000](?infra/v7000).

### Rack A

 * Lâmina 1 - [IP: 10.46.2.2](https://10.46.2.2/ui/#/login)
 * Lâmina 2 - [IP: 10.46.2.3](https://10.46.2.3/ui/#/login)
 * Lâmina 3 - [IP: 10.46.2.4](https://10.46.2.4/ui/#/login)
 * Lâmina 4 - [IP: 10.46.2.5](https://10.46.2.5/ui/#/login)

### Rack B

 * Lâmina 1 - [IP: 10.46.2.11](https://10.46.2.11/ui/#/login)
 * Lâmina 2 - [IP: 10.46.2.12](https://10.46.2.12/ui/#/login)
 * Lâmina 3 - [IP: 10.46.2.13](https://10.46.2.13/ui/#/login)
 * Lâmina 4 - [IP: 10.46.2.14](https://10.46.2.14/ui/#/login)


## Acesso

O acesso às lâminas pode ser feito através do navegador acessando os endereços abaixo ou através do commando SSH com usuário **root**, caso esteja previamente habilitado.

> **ssh** root@<ip da lâmina>


Para Habilitar o acesso ssh, siga os seguintes passos:

   1. Usando um navegador, acesse o gerenciador web através do endereço: **https>//<ip da lâmina>/ui/#/login**. 
   2. Efetue o login com usuário **root**
   3. No painel a direita, click no menu superior **"Actions"**
   4. No submenu **"Services"**, click em *"Enable Secure Shell (SSH)"*
