---
title: Links
---

 * [Guia Foca GNU/Linux](https://guiafoca.org/?page_id=14)
 * [Kubernetes](https://kubernetes.io/pt)
 * [Learn Go](https://golang.org/)
 * [Pico CMS](http://picocms.org/)
 * [Redis](https://redis.io/)
 * [Tomcat + Redis](https://github.com/chexagon/redis-session-manager)
 * [Katacoda](https://www.katacoda.com/)
