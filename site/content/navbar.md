---
title: Navigation bar
---

 - [Home](%base_url%)
 - [Infraestrutura](#)
   - [Servidores](%base_url%?infra/servers)
   - [IBM Blade System](%base_url%?infra/blades)
   - [VMWare](%base_url%?infra/vmware)
   - [Cluster Kubernetes](%base_url%?infra/k8s)
   - [V7000](%base_url%?infra/v7000)
 - [Sistemas](#)
	- [Spede](%base_url%?01-sistemas)
	- [Julgamento](#)
	- [SEI](#)
 - [Procedimentos](#)
	- [Ligar/Desligar Datacenter](%base_url%?01-sistemas)
	- [Implantação de Novos Sistemas](%base_url%?01-sistemas)
	- [Backup](%base_url%?01-sistemas)
	- [Criação de Máquina Virtual](%base_url%?01-sistemas)
 
