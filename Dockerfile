### stage 0 ###

FROM debian

RUN apt-get update && apt-get install -y nginx php php-fpm php-mbstring php-dom

COPY default.nginx /etc/nginx/sites-available/default
COPY cmd.sh /usr/bin/cmd.sh

VOLUME /var/www/html/site

EXPOSE 80

CMD /bin/bash /usr/bin/cmd.sh
